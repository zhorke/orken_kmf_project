# Используем официальный образ Alpine как базовый образ
FROM alpine:latest

# Устанавливаем nginx и создаем директорию для статических файлов
RUN apk add --no-cache nginx
RUN mkdir -p /var/www

# Создаем файл index.html с приветствием
RUN echo "Hello KMF!" > /var/www/index.html

# Копируем конфигурационный файл nginx
COPY nginx.conf /etc/nginx/nginx.conf

# Запускаем nginx
CMD ["nginx", "-g", "daemon off;"]